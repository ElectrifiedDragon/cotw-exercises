Resume Guidelines
(These are our rules for this exercise and not necessarily our general recommendations 
for all resumes.)

##Header:
- Should include name, address, contact information.
- Address and contact information should have one item per line and be single spaced. See 
Clayton Danks examples.

##Content:
- Should at least contain the standard headings: Education, Experience, and Achievements. 
- They can have different names, ie. Work Experience, Honors instead of Achievements.
- However, if there is not enough to list under a heading, e.g. no formal work experience, then 
exclude that heading and consider substituting another heading such as suggested below.
- If space allows, other material is optional, e.g. Skills or Extracurricular Activities 
(that don't fit in other categories). 

##Style:
- You can make *tasteful* changes to css, such as link color, font, list style, alignment 
and spacing, etc.
- But you should maintain black text on white background and keep it simple.
- Keep 1 em margins around the whole document.
- Keep left alignment for content.