### C. Clayton Danks
Thermopolis, WY  
cdanks@steamboat.com  
307 623 1970  

### Education:
Holt County Elementary, 8th grade class of 1895

### Experience:

Skilled ranch hand and successful homesteader

### Honors:

* Three-time winner of Cheyenne Frontier Days

* Best known as emblematic rider of Steamboat on the Wyoming state logo
