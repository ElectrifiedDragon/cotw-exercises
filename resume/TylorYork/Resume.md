### Tylor A. York

1045 1/2 Sheridan Ave.

Sheridan, WY 82801

tylorgoldenyoshi@gmail.com

(307) 429-8192

#### Education
Sheridan Senior High School, Class of 2016 (3.143 GPA)
#### Experience
**Sheridan Public Library**
- **July 2016 - Present** - Assisted in organizing and doing archiving in the Wyoming Room.
#### Skills
**Computer Hardware Repair**
- TestOut PC Pro Certification

**Network Troubleshooting and Management**
- TestOut Network Pro Certification

**MUGEN Content Creation**
- Created characters, stages, and UIs for the MUGEN fighting game engine. Edited characters made by other people for balance.
